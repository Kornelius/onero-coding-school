const mongoose = require('mongoose')

// DB Option
const dbUrl = "mongodb+srv://luxaezar:grandcruz@cluster0-x3zsl.mongodb.net/KucingMaster"
const dbOption = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

mongoose.Promise = global.Promise
mongoose.connect(dbUrl, dbOption)

module.exports = {mongoose}