const express = require('express')
const router = express.Router()

// Model
const { Admins } = require('../models/admin')

// Register
router.post('/register', async function (req, res) {
  try {

    var query = {
      username: req.body.username,
      password: req.body.password,
      name: req.body.name
    }

    const admin = new Admins(query)
    await admin.save()

    res.send('register')
  } catch (e) {
    console.log(e)
  }
})

router.post('/login', async function (req, res) {
  try {
    var adminData = await Admins.findByCredential(req.body.username, req.body.password)

    res.send(adminData)
  } catch (e) {
    console.log(e)
  }
})

module.exports = router