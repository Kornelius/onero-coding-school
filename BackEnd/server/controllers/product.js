const express = require('express')
const router = express.Router()
const multer = require('multer')
const fs = require('fs')

// Model
const { Product } = require('../models/product')

var imagepath = 'tmp/my-uploads/'
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, imagepath)
  },
  filename: function (req, file, cb) {
    var extension = file.originalname.substring(file.originalname.lastIndexOf('.'))
    cb(null, file.fieldname + '-' + Date.now() + extension)
  }
})
var upload = multer({ storage })

// Insert
router.post('/insert', upload.single('productImage'), function (req, res) {
  try {
    // res.send(req.file.filename)

    var img = req.file.filename
    var myobj = {
      productName: req.body.productName,
      productBrand: req.body.productBrand,
      productImage: img,
      productPrice: req.body.productPrice,
      productDescription: req.body.productDescription,
      productBgColor: req.body.productBgColor,
      productFeatured: req.body.productFeatured,
      entryDate: Date.now()
    }
    const product = new Product(myobj)
    product.save()
    res.status(201).send("1 document inserted")
  } catch (error) {
    // res.status(400).send('Error')
    console.log(error)
  }
})

// Find One
router.get('/findOne/:id', async function (req, res) {
  try {
    const id = req.params.id
    var filter = {
      _id: id
    }
    const product = await Product.findOne(filter)
    product.productImage = "http://localhost:3000/img/" + product.productImage
    res.send(product)
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Find All
router.get('/findAll', async function (req, res) {
  try {
    const product = await Product.find({})
    const productData = []
    if (product) {
      for (var index in product) {
        productData.push({
          _id: product[index]._id,
          productName: product[index].productName,
          productBrand: product[index].productBrand,
          productImage: 'http://localhost:3000/img/' + product[index].productImage,
          productPrice: product[index].productPrice,
          productDescription: product[index].productDescription,
          productBgColor: product[index].productBgColor,
          productFeatured: product[index].productFeatured,
          entryDate: product[index].entryDate,
        })
      }
    }
    res.send(productData)
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Find All + Filter
router.get('/findAllFilter', async function (req, res) {
  try {
    var filter = {
      address: req.body.address
    }
    const Product = await Product.find(filter)
    res.send(Product)
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Delete One
router.delete('/deleteOne/:id', async function (req, res) {
  try {
    var myquery = {
      _id: req.params.id
    }

    // var kucing = await Product.findOne(myquery)
    // if (kucing) {
    //   fs.unlink(imagepath + product.image, (err) => {
    //     if (err) throw err;
    //     res.send(product.image);
    //   });
    // }

    var product = await Product.findOne(myquery)
    if (product) {
      // Hapus Image
      fs.unlink(imagepath + product.productImage, (err) => {
        if (err) throw err;
        console.log('path/' + product.productImage + ' was deleted');
      });
    }

    await Product.deleteOne(myquery)
    res.send("1 document deleted")
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Delete Many
router.delete('/deleteMany', async function (req, res) {
  try {
    var myquery = {
      address: "Highway 37 2"
    }
    var result = await Product.deleteMany(myquery)
    res.send(result.n + " document(s) deleted")
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Update One
router.patch('/updateOne/:id', upload.single('productImage'), async function (req, res) {
  try {
    var myquery = {
      _id: req.params.id
    }

    var img = req.file.filename

    var newvalues = {
      $set: {
        productName: req.body.productName,
        productBrand: req.body.productBrand,
        productImage: img,
        productPrice: req.body.productPrice,
        productDescription: req.body.productDescription,
        productBgColor: req.body.productBgColor,
        productFeatured: req.body.productFeatured,
      }
    }

    var product = await Product.findOne(myquery)
    if (product) {
      // Hapus Image
      fs.unlink(imagepath + product.productImage, (err) => {
        if (err) throw err;
        console.log('path/' + product.productImage + ' was deleted');
      });
    }

    await Product.updateOne(myquery, newvalues)
    res.send("1 document updated")
  } catch (error) {
    res.status(400).send('Error')
  }
})

// Update Many
router.put('/updateMany', async function (req, res) {
  try {
    var myquery = {
      name: "Company Inc 2"
    }
    var newvalues = {
      $set: {
        name: "Minnie s"
      }
    }
    var result = await Product.updateMany(myquery, newvalues)
    res.send(result.nModified + " document(s) updated")
  } catch (error) {
    res.status(400).send('Error')
  }
})

module.exports = router