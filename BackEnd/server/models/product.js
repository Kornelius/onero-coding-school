const {mongoose} = require('../config/db')

var product = new mongoose.Schema({
    productName: {
      type: String,
      trim: true,
      required: true
    },
    productBrand: {
      type: String,
      trim: true,
      required: true
    },
    productImage: {
      type: String,
      trim: true,
      required: true
    },
    productPrice: {
      type: String,
      trim: true,
      required: true
    },
    productDescription: {
      type: String,
      trim: true,
      required: true
    },
    productBgColor: {
      type: String,
      trim: true,
      required: true
    },
    productFeatured: {
      type: Boolean,
      required: true
    },
    entryDate: {
      type: Date,
      default: Date.now()
    }
    
  })
  
  var Product = mongoose.model('product', product)

  module.exports = {Product}